#!/bin/python3
import os, stat, sys, getopt
import subprocess

try:
    import apt
except ImportError:
    update = subprocess.run(["sudo", "apt-get", "update"])
    if update.returncode == 0:
        py_apt = subprocess.run(["sudo", "apt-get", "install", "python3-apt"])
        if py_apt.returncode != 0:
            print("python-apt could not be installed")
            sys.exit(1)
        import apt

    

from apt.debfile import DebPackage
import time
import json
# LOGGINGG
import logging
from config_files import Configurator, DEFAULT_CONNECTORS

FORMAT = '%(asctime)s |%(levelname)s| %(funcName)s: %(message)s'

logging.basicConfig(format=FORMAT, level=logging.DEBUG)

configs = Configurator(logging)

cache = apt.cache.Cache()

def check_installed():

    cache.update()
    cache.open()
    logging.info("Checking installed packages...")
    time.sleep(1)
    gw = None
    netplan = cache['netplan.io']
    python3_pip = cache['python3-pip']
    python3_paho = cache['python3-paho-mqtt']
    python3_regex = cache['python3-regex']
    python3_jsonpath_rw = cache['python3-jsonpath-rw']
    broker = cache["mosquitto"]
    NM = cache['network-manager']
    try:
        gw = cache["python3-thingsboard-gateway"]
        logging.debug("GW: " + str(gw.is_installed))
    except KeyError as kerr:
        logging.debug("GW: False")

    logging.debug("netplan: " + str(netplan.is_installed))
    time.sleep(0.2)
    logging.debug("Python3-pip: " + str(python3_pip.is_installed))
    time.sleep(0.5)
    logging.debug("Broker: " + str(broker.is_installed))
    time.sleep(0.5)
    logging.debug("NetworkManager: " + str(NM.is_installed))
    time.sleep(1)


    if not netplan.is_installed:
        netplan.mark_install()

    if not python3_paho.is_installed:
        python3_paho.mark_install()

    if not python3_jsonpath_rw.is_installed:
        python3_jsonpath_rw.mark_install()

    if not python3_regex.is_installed:
        python3_regex.mark_install()

    if not python3_pip.is_installed:
        python3_pip.mark_install()

    if not broker.is_installed:
        broker.mark_install()

    if not NM.is_installed:
        NM.mark_install()

    _install_packages()

    if gw == None:
        _download_and_install_gateway()

def install_pip_packages():
    ret = subprocess.run(["pip3", "install", "-r", "requirements.txt"])
    if ret.returncode != 0:
        logging.error("Error installing pip packages rc: %d" % ret.returncode)
        sys.exit(1)


def _install_packages(full=True):
    logging.debug("Installing Packages!")

    try:
        cache.commit()
    except Exception as e:
        logging.error(e)
        sys.exit(1)

    install_pip_packages()
    if full:
        _enable_networkManager()
        _enable_broker()
    cache.close()

# 1. Download TB-GW deb file
def _download_and_install_gateway():

    logging.info("Downloading gateway package")
    time.sleep(2)

    os.system("wget -O tb_gateway.deb https://github.com/thingsboard/thingsboard-gateway/releases/latest/download/python3-thingsboard-gateway.deb")

    logging.info("Download SUCCESS")
    logging.info("Installing...")
    time.sleep(0.5)
    
    try:

        deb = DebPackage("./tb_gateway.deb")
        # logging.debug(deb.depends)
        # cache.open()
        # for depend in deb.depends:
            # if depend[0][0] != "python3:any":
                # cache[depend[0][0]]
            # else:
                # cache[depend[0][0].split(":")[0]]
        # _install_packages(False)
        # logging.debug("waiting 5 seconds")
        # time.sleep(5)
        ret = deb.install()
        if not ret > 0:
            logging.info("Installation Complete with return code: " + str(ret))
    except Exception as e:

        logging.exception(e)
    
def _enable_broker():

    try:
        os.system("sudo systemctl enable mosquitto")
        configs.install_broker()
    except Exception as e:
        logging.exception(e)

def _enable_networkManager():

    try:
        configs.install_networkManager()
    except Exception as e:
        logging.exception(e)

# DEPRACTED
def nune(device):
    res = ""
    res = [ _connector[device] for _connector in DEFAULT_CONNECTORS ]
    logging.debug(str(_connector))
    
    if res == "":
        raise "connector type not found"


    try:
        with open(res) as f:
            _config = json.load(f)
            logging.debug(str(_config))
    


        with open("./device_map.json") as dm_f:
            device_map = json.load(dm_f)
            logging.debug(str(device_map))
    except FileNotFoundError as fnferr:
        logging.exception("File not found: " + fnferr.filename)

    except Exception as e:
        logging.exception(e)

def restart_gateway():
    os.system("sudo systemctl restart thingsboard-gateway")

def main(argv):
    small_options = "hcbngp"
    long_options = ["help", "network-manager", "gateway","fix-permissions"]
    try:
        opts, args = getopt.getopt(argv, small_options, long_options )
        opts_count = len(opts)
        logging.debug(opts_count)
        logging.debug(argv)

        # Install if accessToken is supplied
        if opts_count == 0 and argv[0] != '':
            # 1. Check and install required packages
            check_installed()
            # 2. Write cusom configs
            configs.install_configs()
            # 3. Write custom TB.yaml
            configs.install_tb_config(argv[0])
            # 4. Copy converters to correct position
            configs.install_converters()
            # 5. Fix Permissions
            configs.fix_permissions()
            # 6. Restart Gateway
            restart_gateway()
        

        for opt, arg in opts:
            if opt in ('-h','--help'):
                print(_menu)
            elif opt == '-c':
                configs.install_converters()
            elif opt == '-b':
                _download_and_install_broker()
            elif opt in ('-n', '--network-manager'):
                _download_and_install_networkManager()
            elif opt in ('-g', '--gateway'):
                _download_and_install_gateway()
            elif opt in ('-p', '--fix-permissions'):
                configs.fix_permissions()


    except IndexError as ierr:
        logging.error("AccessToken is missing")
            

    except getopt.GetoptError as gerr:
        logging.error(_menu)


_menu = """Usage: 
 ./install.py <AccessToken>         : Install FULL gateway
 ./install.py -c                    : Install Converters
 ./install.py -n --network-manager  : Install NetworkManager
 ./install.py -b                    : Install Mosquitto Broker
 ./install.py -h --help             : Shows this help
 ./insatll.py -g --gateway          : Install only tb_gateway
 ./install.py -p --fix-permissions  : Fix permissions on TBG folders"""

if __name__ == '__main__':
    main(sys.argv[1:])


