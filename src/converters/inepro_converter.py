from pymodbus.payload import BinarytPayloadDecoder

from thingsboard_gateway.connectors.modbus.modbus_converter import ModbusConverter, log
import logging
from simplejson import dumps

class IneproConverter(ModbusConverter):
    def __init__(self, config):
        self.dict_result = {}


    def convert(self, config, data):
        self.__config = config
        log.info("TestModbusConverter Data:" + str(data) + " Config: " +  dumps(self.__config))
        self.dict_result["deviceName"] = self.__config["deviceName"]
        self.dict_result["deviceType"] = "None"

        # NOTE: add deviceType to modbus.json
        # self.dict_result["deviceType"] = self.__config["deviceType"]

        self.dict_result["telemetry"] = []
        self.dict_result["attributes"] = []
        return self.dict_result
