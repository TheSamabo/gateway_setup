import os, time
import shutil
import json
import sys
import yaml
import subprocess

TB_USER = "thingsboard_gateway"
TB_GROUP = "thingsboard_gateway"
MOSQUITTO_CONF = "./conf/mosquitto.conf"
EXTENSION_PATH = "/var/lib/thingsboard_gateway/extensions"

DEFAULT_GW_YAML  = "/etc/thingsboard-gateway/config/tb_gateway.yaml"
CUSTOM_GW_YAML = "./conf/tb.yaml"

DEFAULT_CONNECTORS = [
            # Mqtt
            {"mqtt": "/etc/thingsboard-gateway/config/mqtt.json"}
        ]

CUSTOM_CONNECTORS = [
        
            # Mqtt
            {"mqtt": "./conf/mqtt.json"}
        ]

class Configurator:
    def __init__(self, logging):
        self.logging = logging

    def _fix_permission(self, path):
        try:
            shutil.chown(path, TB_USER, TB_GROUP)
        except Exception as e:
            self.logging.error(e)

    def fix_permissions(self):
        self.logging.info("Fixing permissions!")
        # change user and group of dirs to thingsboard_gateway
        # Extensions/Converters
        extension_root_folder = "/var/lib/thingsboard_gateway"
        self._fix_permission(extension_root_folder)
        self._fix_permission(EXTENSION_PATH)

        _exts = os.scandir(EXTENSION_PATH)
        self.logging.debug(_exts)
        for ext in _exts:
            self.logging.debug(ext.path + " is dir: " +str(ext.is_dir()))
            self._fix_permission(ext.path)
            if bool(ext.is_dir()):
                # List of files
                connector_files = os.scandir(ext.path)
                for _file in connector_files:
                    self._fix_permission(_file.path)
                    self.logging.debug(_file.path)

        root_tb_folder = "/etc/thingsboard-gateway"
        self._fix_permission(root_tb_folder)
        
        _root = os.scandir(root_tb_folder)
        self.logging.debug(_root)
        for root in _root:
            self.logging.debug(root.path + " is dir: " +str(root.is_dir()))
            self._fix_permission(root.path)
            if bool(root.is_dir()):
                # List of files
                root_files = os.scandir(root.path)
                for _file in root_files:
                    self._fix_permission(_file.path)
                    self.logging.debug(_file.path)

        ### Default path to config files
    def get_connector_path(self, connector, default=True):

        _connector_list = DEFAULT_CONNECTORS
        if not default:
            _connector_list = CUSTOM_CONNECTORS

        _path = ""
        for config in _connector_list:
            self.logging.debug("Config: " + str(config))
            for key, val in config.items():
                if connector == key:
                    _path = val
                elif key in connector.keys():
                    _path = val
        if _path == "":
            self.logging.error("Connector not found!")
        else:
            return _path



    def get_connector_name(self, connector):

        if isinstance(connector, dict):
            for key , val in connector.items():
                return key
                
        else:
            self.logging.error("Connector is not type dictionary!")
            return None


    def clear_mappings(self):

        for connector in DEFAULT_CONNECTORS:
            try:
                _path = self.get_connector_path(connector)

                with open(_path,"r+") as conf_f:
                    json_config = json.load(conf_f)
                    json_config["mapping"] = []
                    conf_f.truncate(0)
                    conf_f.seek(0)
                    
                    json.dump(json_config, conf_f, indent=4)
                    conf_f.close()
                     
                self.logging.debug("Mapping for connector: " + connector + " are cleared")

            except FileNotFoundError as ferr:
                self.logging.exception(ferr)

    def install_configs(self):

        for connector in CUSTOM_CONNECTORS:
            try:
                _path_custom = self.get_connector_path(connector, default=False)
                _path_tb = self.get_connector_path(connector)


                # Open and read custom config for that connector
                with open(_path_custom) as cf:
                    custom_config = json.load(cf)
                    
                with open(_path_tb, "w") as tbf:
                    tbf.truncate(0)
                    tbf.seek(0)
                    json.dump(custom_config, tbf, indent=2)
                cf.close()
                tbf.close()
            except FileNotFoundError as ferr:
                self.logging.exception("File not found")

    def install_tb_config(self, accesstoken):
        self.logging.debug("AccessToken supplied: " + str(accesstoken))
        with open(CUSTOM_GW_YAML) as cf:
            conf = yaml.full_load(cf)
            conf["thingsboard"]["security"]["accessToken"] = accesstoken

        with open(DEFAULT_GW_YAML, 'w') as tb_f:
            yaml.dump(conf, tb_f, indent=2)
        tb_f.close()
        cf.close()
        self.logging.debug(conf)

    ### Copy converters from 'converters/' to correct connector extension folder 
    def install_converters(self):

        converters = "./converters"

        try:
            dirs = os.listdir(converters)
            self.logging.debug("Directories found: " + str(dirs))
            for _dir in dirs:
                _files = os.listdir(converters + "/" + _dir)
                for _file in _files:
                    shutil.copy2(converters + "/" + _dir + "/" + _file, \
                            EXTENSION_PATH + "/" + _dir)
                    self.logging.debug("Copied: " + str(_file) + " to: " + \
                            EXTENSION_PATH + "/" + _dir)
            
            shutil.chown("/var/lib/thingsboard_gateway", "thingsboard_gateway", "thingsboard_gateway")

        except FileNotFoundError as ferr:
            self.logging.error(ferr)


    def install_broker(self):
        self.logging.debug("Coping MQTT conf")
        shutil.copy2(MOSQUITTO_CONF, "/etc/mosquitto")
        os.system("sudo systemctl restart mosquitto")
        

    def install_networkManager(self):
        # Backup old Netplan yaml file
        self.logging.debug("Backing up old netplan config")
        shutil.copy2("/etc/netplan/50-cloud-init.yaml", "/etc/netplan/50-cloud-init.yaml.backup")
        
        # Install preconfigured netplan config
        self.logging.debug("Installing preconfigured netplan config")
        shutil.copy2("./network/50-cloud-init.yaml", "/etc/netplan/")

        self.logging.debug("Trying netplan config")
        ret = subprocess.run(['sudo', 'netplan', 'try'])

        self.logging.debug("Applying netplan config")
        ret = subprocess.run(['sudo', 'netplan', 'apply'])



        


        
                    




        

    

        




